import pytest
from django.urls import reverse
from rest_framework.authtoken.models import Token

from market.models import Product, UserReview


@pytest.fixture
def product():
    product = Product.objects.create(name='test', description='test desc')
    return product


@pytest.fixture
def three_products():
    Product.objects.create(name='test', description='test desc')
    Product.objects.create(name='test2', description='test desc2')
    Product.objects.create(name='test3', description='test desc3')


@pytest.fixture
def review(product, admin_user):
    review = UserReview.objects.create(product=product, user=admin_user, rating=1)
    return review


@pytest.fixture
def two_reviews(product, admin_user):
    first_review = UserReview.objects.create(product=product, user=admin_user, rating=4)
    last_review = UserReview.objects.create(product=product, user=admin_user, rating=5)
    return first_review, last_review


@pytest.fixture
def token(admin_user):
    token = Token.objects.create(key='test_token', user=admin_user)
    return token.key


def test_ping(client):
    url = reverse('api-v1:service-ping')
    response = client.get(url)

    assert response.status_code == 200
    assert response.json()['pong'] == 'ok'


@pytest.mark.django_db
class TestProductViewSet:
    @pytest.mark.parametrize('action, kwargs', [
        ('list', {}),
        ('detail', {'pk': 1}),
    ])
    def test_unauthorized(self, client, action, kwargs):
        url = reverse(f'api-v1:products-{action}', kwargs=kwargs)
        response = client.get(url)

        assert response.status_code == 401

    def test_list(self, client, three_products, token):
        url = reverse('api-v1:products-list')
        response = client.get(url, HTTP_AUTHORIZATION=f'Token {token}')

        assert response.status_code == 200
        response_data = response.json()
        assert response_data['count'] == 3

    def test_retrieve(self, client, product, token):
        url = reverse('api-v1:products-detail', kwargs={'pk': product.pk})
        response = client.get(url, HTTP_AUTHORIZATION=f'Token {token}')

        assert response.status_code == 200


@pytest.mark.django_db
class TestUserReviewNestedViewSet:
    @pytest.mark.parametrize('action, kwargs', [
        ('list', {'product_pk': 1}),
        ('detail', {'product_pk': 1, 'pk': 1}),
    ])
    def test_unauthorized(self, client, action, kwargs):
        url = reverse(f'api-v1:product-reviews-{action}', kwargs=kwargs)
        response = client.get(url)

        assert response.status_code == 401

    def test_list(self, client, two_reviews, token):
        url = reverse('api-v1:product-reviews-list', kwargs={'product_pk': two_reviews[0].product.pk})
        response = client.get(url, HTTP_AUTHORIZATION=f'Token {token}')

        assert response.status_code == 200
        response_data = response.json()
        assert response_data['count'] == 2

    def test_retrieve(self, client, review, token):
        url = reverse('api-v1:product-reviews-detail', kwargs={'product_pk': review.product.pk, 'pk': review.pk})
        response = client.get(url, HTTP_AUTHORIZATION=f'Token {token}')

        assert response.status_code == 200

    def test_create_201(self, client, product, token):
        url = reverse('api-v1:product-reviews-list', kwargs={'product_pk': product.pk})
        data = {
            'rating': 5,
            'comment': 'test_comment',
        }
        response = client.post(url, data=data, content_type='application/json', HTTP_AUTHORIZATION=f'Token {token}')

        assert response.status_code == 201

        response_data = response.json()
        assert response_data['product'] == product.pk
        assert response_data['rating'] == data['rating']
        assert response_data['comment'] == data['comment']

    def test_create_non_unique_400(self, client, product, token):
        url = reverse('api-v1:product-reviews-list', kwargs={'product_pk': product.pk})
        data = {
            'rating': 5,
            'comment': 'test_comment',
        }
        # Two similar requests, first must return 201 as created, second - 400 as not unique
        response = client.post(url, data=data, content_type='application/json', HTTP_AUTHORIZATION=f'Token {token}')
        assert response.status_code == 201

        response = client.post(url, data=data, content_type='application/json', HTTP_AUTHORIZATION=f'Token {token}')
        assert response.status_code == 400

        response_data = response.json()
        assert 'non_field_errors' in response_data

    def test_patch(self, client, review, token):
        url = reverse('api-v1:product-reviews-detail', kwargs={'product_pk': review.product.pk, 'pk': review.pk})
        data = {
            'rating': 5,
        }

        response = client.patch(url, data=data, content_type='application/json', HTTP_AUTHORIZATION=f'Token {token}')
        print(response.json())
        assert response.status_code == 200

    def test_put(self, client, review, token):
        url = reverse('api-v1:product-reviews-detail', kwargs={'product_pk': review.product.pk, 'pk': review.pk})
        data = {
            'rating': 5,
            'comment': 'best',
        }

        response = client.put(url, data=data, content_type='application/json', HTTP_AUTHORIZATION=f'Token {token}')
        print(response.json())
        assert response.status_code == 200

    def test_delete(self, client, review, token):
        url = reverse('api-v1:product-reviews-detail', kwargs={'product_pk': review.product.pk, 'pk': review.pk})

        response = client.delete(url, HTTP_AUTHORIZATION=f'Token {token}')
        assert response.status_code == 204

