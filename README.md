# Django Simple Market (0.2.0)
## Pre-requirements
* [Python ^3.7](https://www.python.org) - language, run-time
* [Poetry](https://poetry.eustace.io) - deps management 
* [Docker](https://www.docker.com/) - container, build system
* [docker-compose](https://docs.docker.com/compose/) - service orchestration
## Overview
This project was made for testing and learning purposes.
I must admit that it came a little bit over-complicated, but it was 
fun and interesting to test things like `django-split-settings` and
Docker/CI stuff from [we-make-services](https://github.com/wemake-services/wemake-django-template) template.


Market is consists with `Users`, `Products` and `UserReviews`.

`Users` can sign up and sign in to the market, watch list of `Products` and leave reviews on them (rating and comment)

Admin user can create new products, update them and delete.
## Installation
First, clone the project:

    git@gitlab.com:just13kiy/django-simple-market.git
    
If your python version is below 3.7 then do following

    pyenv install 3.7.4
    pyenv local 3.7.4
    
 Install dependencies:
 
    poetry install
    
Vouila! You're ready to start.

## Running
### Local
You must have `Postgres 11` and `RabbitMQ` running localy

Preparing server for running:

    python ./manage.py migrate
    python ./manage.py collectstatic

Running server:
    
    python ./manage.py runserver 

Running celery workers:

    celery worker --app django_simple_market -Q default --loglevel debug
    
 This is it, server is running
 
### Docker
This one should be pretty simple:

    docker-compose build
    docker-compose run --rm web python manage.py migrate
    docker-compose up
    
This commands will up whole stack (Postgres, Rabbit, Redis) and run server.

