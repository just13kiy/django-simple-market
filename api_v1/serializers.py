import typing as t

from django.contrib.auth.models import User

from rest_framework import serializers
from rest_framework.validators import UniqueTogetherValidator

from market.models import Product, UserReview


class ProductSerializer(serializers.ModelSerializer):
    user_rating = serializers.SerializerMethodField()

    class Meta:
        model = Product
        fields = serializers.ALL_FIELDS
        read_only_fields = [
            'created_at', 'updated_at',
        ]

    def get_user_rating(self, instance: 'Product') -> t.Optional[int]:
        product_id = instance.pk
        user = self.context['request'].user
        try:
            user_review = UserReview.objects.get(product=product_id, user=user)
            return user_review.rating
        except UserReview.DoesNotExist:
            return None


class UserReviewSerializer(serializers.ModelSerializer):
    product = serializers.PrimaryKeyRelatedField(queryset=Product.objects.all())
    user = serializers.PrimaryKeyRelatedField(queryset=User.objects.all())

    class Meta:
        model = UserReview
        fields = serializers.ALL_FIELDS
        read_only_fields = [
            'created_at', 'updated_at',
        ]
        validators = [
            UniqueTogetherValidator(
                queryset=UserReview.objects.all(),
                fields=['product', 'user']
            )
        ]


# TODO: investigate a DRY-way
class UserReviewUpdateSerializer(serializers.ModelSerializer):
    product = serializers.PrimaryKeyRelatedField(queryset=Product.objects.all())
    user = serializers.PrimaryKeyRelatedField(queryset=User.objects.all())

    class Meta:
        model = UserReview
        fields = serializers.ALL_FIELDS
        read_only_fields = [
            'product', 'user',
            'created_at', 'updated_at',
        ]
