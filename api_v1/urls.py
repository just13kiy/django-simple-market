from django.conf.urls import url

from rest_framework_nested import routers

from . import views

app_name = 'api_v1'


# BUILD URLPATTERNS ----------------------------------------------------------------------------------------------------
router = routers.SimpleRouter()
router.register(r'products', views.ProductViewSet, base_name='products')

user_reviews_router = routers.NestedSimpleRouter(router, r'products', lookup='product')
user_reviews_router.register(r'reviews', views.UserReviewsNestedViewSet, base_name='product-reviews')

urlpatterns = [
    url(r'^ping/$', views.ping, name='service-ping'),
]

urlpatterns += router.urls
urlpatterns += user_reviews_router.urls
