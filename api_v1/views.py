import typing as t

from django.shortcuts import get_object_or_404
from django.utils.functional import cached_property

from rest_framework import permissions, viewsets, status
from rest_framework.decorators import api_view, authentication_classes, permission_classes
from rest_framework.exceptions import NotFound
from rest_framework.response import Response

from market.models import Product, UserReview

from . import serializers

if t.TYPE_CHECKING:
    from django.db.models.query import QuerySet
    from rest_framework.request import Request


@api_view()
@authentication_classes([])
@permission_classes((permissions.AllowAny,))
def ping(*_) -> 'Response':
    return Response(data={'pong': 'ok'})


class ProductMixin:
    @cached_property
    def product(self) -> 'Product':
        """
        Returns `Product` instance.

        ...note:
            +1 DB hit
        """
        try:
            # noinspection PyUnresolvedReferences
            return get_object_or_404(Product, pk=self.kwargs['product_pk'])
        except ValueError:
            # NOTE: `product_pk` is not id, thus return 404 (like "django" does)
            raise NotFound


class ProductViewSet(viewsets.ReadOnlyModelViewSet):
    serializer_class = serializers.ProductSerializer

    def get_queryset(self) -> 'QuerySet':
        queryset = Product.objects.prefetch_related('reviews')
        return queryset


class UserReviewsNestedViewSet(ProductMixin, viewsets.ModelViewSet):
    serializer_class = serializers.UserReviewSerializer

    def get_queryset(self) -> 'QuerySet':
        return UserReview.objects.filter(product=self.product)

    def create(self, request: 'Request', *args: t.Any, **kwargs: t.Any) -> 'Response':
        data = dict(request.data)
        data['user'] = request.user.pk
        data['product'] = self.product.pk

        serializer = self.get_serializer(data=data)
        serializer.is_valid(raise_exception=True)
        serializer.save()

        return Response(serializer.data, status=status.HTTP_201_CREATED)

    def update(self, request: 'Request', *args, **kwargs):
        # TODO: this is wet, should be DRY (badum-ts)
        data = dict(request.data)
        data['user'] = request.user.pk
        data['product'] = self.product.pk

        serializer = serializers.UserReviewUpdateSerializer(instance=self.get_object(), data=data)
        serializer.is_valid(raise_exception=True)
        serializer.save()

        return Response(serializer.data, status=status.HTTP_200_OK)
