from rest_framework.authentication import SessionAuthentication
from rest_framework.permissions import IsAdminUser

from drf_yasg import openapi
from drf_yasg.views import get_schema_view


def get_swagger_schema_view(patterns):
    # just a shortcut, nothing special
    description = (
        '`$ curl http://example.com/api/v1/ping -H "Authorization: Bearer <token>"`'
    )
    schema_view = get_schema_view(
        openapi.Info(
            title='Django Simple Market API',
            default_version='v1',
            description=description,
        ),
        public=True,
        authentication_classes=(SessionAuthentication,),
        permission_classes=(IsAdminUser,),
        patterns=patterns
    )

    return schema_view.with_ui('swagger', cache_timeout=0)
