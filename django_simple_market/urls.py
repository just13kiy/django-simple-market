from django.contrib import admin
from django.urls import include, path

from api_v1 import get_swagger_schema_view as api_v1_swagger

api_v1_urlpatterns = [
    path('v1/', include('api_v1.urls', namespace='api-v1')),
]

urlpatterns = [
    path('admin/', admin.site.urls),
    path('admin/uwsgi/', include('django_uwsgi.urls')),

    path('docs-v1/', api_v1_swagger(api_v1_urlpatterns)),
    path('rest-auth/', include('rest_auth.urls')),
    path('rest-auth/registration/', include('rest_auth.registration.urls'))
]

urlpatterns += api_v1_urlpatterns
