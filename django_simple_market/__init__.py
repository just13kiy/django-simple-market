__version__ = '0.2.0'

# This will make sure the app is always imported when
# Django starts so that shared_task will use this app.
from django_simple_market.celery_app import app as celery_app  # NOQA
