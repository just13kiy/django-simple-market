import sys

from django.core import management


class JSONExporter:
    def __call__(self, filename: str, *args, **kwargs):
        """
        Dumps all database about products and user reviews into given file
        """
        sysout = sys.stdout
        sys.stdout = open(f'{filename}.json', 'w')
        management.call_command('dumpdata', 'market')
        sys.stdout = sysout
