from django_simple_market import celery_app as app
from celery.schedules import crontab
from .features import JSONExporter


@app.task
def dump_database(filename: str):
    exporter = JSONExporter()
    exporter(filename)


app.conf.beat_schedule = {
    "dump-database-task": {
        "task": "market.tasks.dump_database",
        "schedule": crontab(0, 0, day_of_week=1)
    }
}
