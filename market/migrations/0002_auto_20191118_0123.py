# Generated by Django 2.2.7 on 2019-11-18 01:23

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('market', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='product',
            name='image',
            field=models.ImageField(blank=True, help_text='Image representation of the product', upload_to=''),
        ),
    ]
