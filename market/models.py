from django.contrib.auth.models import User
from django.db import models

from model_utils.fields import AutoCreatedField, AutoLastModifiedField


class TimeStampedModel(models.Model):
    """
    Abstract model. Can be used as a base model to include auto timestamps
    (created_at and updated_at) in every child model.
    """
    created_at = AutoCreatedField('created at', db_index=True)
    updated_at = AutoLastModifiedField('updated at', db_index=True)

    class Meta:
        abstract = True


class Product(TimeStampedModel):
    name = models.CharField(max_length=50, null=False, blank=False, help_text='The name of the product')
    description = models.TextField(null=False, blank=True, help_text='Description of the product')
    image = models.ImageField(null=False, blank=True, help_text='Image representation of the product')

    def __str__(self):
        return f'{self.name}'


class UserReview(TimeStampedModel):
    product = models.ForeignKey(Product, on_delete=models.CASCADE, related_name='reviews')
    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name='reviews')
    rating = models.IntegerField(verbose_name='user\'s rating', blank=False, null=False,
                                 help_text='Rating from user to product')
    comment = models.TextField(max_length=256, blank=True, null=False, help_text='Comment from user about product')
