from django.contrib import admin

from .models import Product, UserReview


@admin.register(Product)
class ProductAdmin(admin.ModelAdmin):
    list_display = (
        'id', 'name', 'description',
        'created_at', 'updated_at',
    )


@admin.register(UserReview)
class UserReviewAdmin(admin.ModelAdmin):
    list_display = (
        'product', 'user',
        'created_at', 'updated_at',
    )
